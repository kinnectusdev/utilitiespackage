//
//  UIImageView_Extensions.swift
//  Utilities
//
//  Created by blakerogers on 3/20/19.
//  Copyright © 2019 blakerogers. All rights reserved.
//

import UIKit
public extension UIImageView {
    /// <#Description#>
    ///
    /// - Parameters:
    ///   - mode: <#mode description#>
    ///   - cornerRadius: <#cornerRadius description#>
    ///   - maskBounds: <#maskBounds description#>
    /// - Returns: <#return value description#>
    static func view(mode: UIView.ContentMode = .scaleAspectFill, cornerRadius: CGFloat = 0, maskBounds: Bool = true) -> UIImageView {
        let image = UIImageView()
        image.contentMode = mode
        image.layer.cornerRadius = cornerRadius
        image.layer.masksToBounds = maskBounds
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }
    /// <#Description#>
    ///
    /// - Returns: <#return value description#>
    func session()-> URLSession{
        let sessDefined = Foundation.URLSession.shared
        return sessDefined
    }
    /// <#Description#>
    ///
    /// - Parameters:
    ///   - data: <#data description#>
    ///   - response: <#response description#>
    ///   - error: <#error description#>
    func getImageHandler(data: Data?,response: URLResponse?,error: Error?) {
        if error == nil{
            if let dataImage = UIImage(data: data!) {
                imageCache.setObject(dataImage, forKey: response!.url!.absoluteString as AnyObject)
                DispatchQueue.main.async {
                    self.image = dataImage
                    if let loader = self.viewWithTag(33) as? UIActivityIndicatorView{
                        loader.stopAnimating()
                        loader.removeFromSuperview()
                    }
                }
            }
        }
    }
    
    /// <#Description#>
    ///
    /// - Parameter url: <#url description#>
    func loadImageWithURL(url: String, defaultImage: UIImage) {
        if let image = imageCache.object(forKey: url as AnyObject) {
            DispatchQueue.main.async {
                self.image = image as UIImage
            }
        } else {
            let url = URL(string:url)
            if url != nil{
                let task = session().dataTask(with: url!, completionHandler: getImageHandler)
                task.resume()
            } else {
                self.image = defaultImage
            }
        }
    }
    /// <#Description#>
    ///
    /// - Parameter view: <#view description#>
    /// - Returns: <#return value description#>
    func withScreen(view: UIView) -> UIImageView {
        self.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.constrainInView(view: self, top: 0, left: 0, right: 0, bottom: 0)
        return self
    }
}
public extension UIImageView {
    /// Create and image view with custom radius, alpha and interaction
    ///
    /// - Parameters:
    ///   - radius: CGFloat - Default 0.0
    ///   - visible: Bool - Default true
    ///   - interactionOn: Bool - Default false
    /// - Returns: UIImageView
    static func image(image: UIImage? = nil, mode: UIView.ContentMode = .scaleAspectFit, radius: CGFloat = 0.0, visible: Bool = true, interactionOn: Bool = false, tintColor: UIColor? = nil) -> UIImageView {
        let imageView = UIImageView()
        imageView.alpha = visible ? 1.0 : 0.0
        imageView.contentMode = mode
        if let color = tintColor {
            imageView.tintColor = color
            imageView.image = image?.withRenderingMode(.alwaysTemplate)
        } else {
            imageView.image = image
        }
        imageView.isUserInteractionEnabled = interactionOn
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = radius
        imageView.backgroundColor = UIColor.clear
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }
    /// Provide size of an image if scaled by vertical or horizontal axis
    ///
    /// - Parameters:
    ///   - frame: CGSize
    ///   - usingHeightRatio: Bool default true
    /// - Returns: CGSize
    func aspectSizeInFrame(frame: CGSize, usingHeightRatio: Bool = true) -> CGSize {
        guard let image = self.image else { return CGSize.zero}
        let imageSize = image.size
        let myImageWidth = imageSize.width
        let myImageHeight = imageSize.height
        let myViewWidth = frame.width
        let myViewHeight = frame.height
        let ratio = myViewWidth/myImageWidth
        let ratio2 = frame.height/myImageHeight
        let scaledHeight = myImageHeight * ratio
        let scaledWidth = myImageWidth * ratio2
        let heightScaledSize: CGSize = CGSize(width: myViewWidth, height: scaledHeight)
        let widthScaledSize: CGSize = CGSize(width: scaledWidth, height: myViewHeight)
        return usingHeightRatio ? heightScaledSize : widthScaledSize
    }
}
