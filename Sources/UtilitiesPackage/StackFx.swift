//
//  StackFx.swift
//  Meld
//
//  Created by blakerogers on 2/29/20.
//  Copyright © 2020 com.meld.kinnectus. All rights reserved.
//

import UIKit

public let stackFillAlignment: View = { view in
    (view as? UIStackView)?.alignment = .fill
    return view
}
public let stackVerticallyAligned: View = { view in
    (view as? UIStackView)?.axis = .vertical
    return view
}
public let stackDistributionEqual: View = { view in
    (view as? UIStackView)?.distribution = .equalSpacing
    return view
}
public let asStackView: (UIView) -> UIStackView = { view in
    guard let view = view as? UIStackView else { fatalError("not a stack View")}
    return view
}
