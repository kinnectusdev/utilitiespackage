//  ActionTextView.swift
//  Meld
//
//  Created by Blake Rogers on 4/24/22.
//  Copyright © 2022 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UIKit

class ActionTextView: UITextView {
    typealias DidChange = () -> Void
    typealias DidBeginEditing = () -> Void
    typealias DidEndEditing = () -> Void
    
    private var didChange: DidChange?
    private var didBeginEditing: DidBeginEditing?
    private var didEndEditing: DidEndEditing?
}

extension ActionTextView {
    func setActions(didChange: DidChange? = nil, didBeginEditing: DidBeginEditing? = nil, didEndEditing: DidEndEditing? = nil) {
        self.delegate = self
        self.didChange = didChange
        self.didBeginEditing = didBeginEditing
        self.didEndEditing = didEndEditing
    }
}

extension ActionTextView: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        didChange?()
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        didBeginEditing?()
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        didEndEditing?()
    }
}
