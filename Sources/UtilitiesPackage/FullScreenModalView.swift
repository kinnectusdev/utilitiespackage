//
//  FullScreenModalView.swift
//
//
//  Created by blakerogers on 11/2/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import UIKit
import Photos

public func dismissButton(_ title: String, _ action: @escaping () -> Void) -> UIButton {
    return ActionButton(action: action)
        |> named("dismiss_button")
        >>> utilizeConstraints
        >>> width_Height(100, 50)
        >>> styledTitle(silverAttributedString(title, 15))
        >>> roundedGreyShadowed(25)
        >>> asButton
}

public func defaultAccountImage() -> UIImage {
    let imageNames = (1...7).map { "default_profile\($0)"}
    let image = imageNames.itemAt(Int(arc4random() % 6)) ?? imageNames.first!
    return UIImage(named: image) ?? UIImage()
}
public let full_screen_modal: (String) -> UIView = { alertText in
 
    let okayButton = dismissButton("OKAY", {})
    
    let view = UIView()
        |> framed(UIScreen.main.bounds)
        >>> blurredWhiteBackground
        >>> appendSubView(titleLabel(alertText)
            |> named("title")
            >>> alignToCenterY(-100)
            >>> alignedToCenterX)
        >>> appendSubView(okayButton
            |> alignedToCenterY
            >>> alignedToCenterX)
    okayButton.addTapGestureRecognizer {
        UIView.animate(withDuration: 0.25, animations: {
            view.alpha = 0.0
        }, completion: { _ in
            view.removeFromSuperview()
        })
    }
    view.addTapGestureRecognizer {
        UIView.animate(withDuration: 0.25, animations: {
            view.alpha = 0.0
        }, completion: { _ in
            view.removeFromSuperview()
        })
    }
    return view
}
public let imbed_full_screen_modal: (UIView) -> UIView = { view in
    let backButton = dismissButton("X") {
        UIView.animate(withDuration: 0.25, animations: {
            view.alpha = 0.0
        }, completion: { _ in
            view.removeFromSuperview()
        })
    }
    
   
    
    return UIView()
        |> framed(UIScreen.main.bounds)
        >>> blurredWhiteBackground
        >>> appendSubView(backButton)
        >>> appendSubView(view |> fill)
    
}
public let full_screen_modal_interactive: (String, @escaping () -> Void, @escaping () -> Void) -> UIView = { alertText, affirmAction, denyAction in
 
    let affirmButton = dismissButton("Okay", {})
    let denyButton = dismissButton("Cancel", {})

    let view = UIView()
        |> framed(UIScreen.main.bounds)
        >>> blurredWhiteBackground
        >>> appendSubView(titleLabel(alertText)
            |> named("title")
            >>> alignToCenterY(-100)
            >>> alignedToCenterX)
        >>> appendSubView(affirmButton
            |> alignedToCenterY
            >>> alignedToCenterX)
    affirmButton.addTapGestureRecognizer {
        
        UIView.animate(withDuration: 0.25, animations: {
            view.alpha = 0.0
        }, completion: { _ in
            affirmAction()
            view.removeFromSuperview()
        })
    }
    denyButton.addTapGestureRecognizer {
        UIView.animate(withDuration: 0.25, animations: {
            view.alpha = 0.0
        }, completion: { _ in
            denyAction()
            view.removeFromSuperview()
        })
    }
    view.addTapGestureRecognizer {
        UIView.animate(withDuration: 0.25, animations: {
            view.alpha = 0.0
        }, completion: { _ in
            view.removeFromSuperview()
        })
    }
    return view
}
