//
//  ActionField.swift
//  Meld
//
//  Created by Blake Rogers on 4/25/22.
//  Copyright © 2022 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UIKit


class ActionField: UITextField {
    typealias DidChange = (String) -> Void
    
    private var didChange: DidChange?
}

extension ActionField {
    func setAction(didChange: DidChange? = nil) {
        self.delegate = self
        self.didChange = didChange
    }
}

extension ActionField: UITextFieldDelegate {
        
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        didChange?(textField.text ?? "")
        return true
    }
}

