//
//  Array_Extensions.swift
//  Utilities
//
//  Created by blakerogers on 3/20/19.
//  Copyright © 2019 blakerogers. All rights reserved.
//

import Foundation
public extension Array where Element: Equatable {
    /// <#Description#>
    ///
    /// - Parameter element: <#element description#>
    /// - Returns: <#return value description#>
    func removing( element: Element) -> Array {
        let tempArray = self.filter { $0 != element}
        return tempArray
    }
    
}

public extension Array {
    ///
    ///
    /// - Parameter index: <#index description#>
    /// - Returns: <#return value description#>
    func itemAt(_ index: Int) -> Element? {
        if count > index && index >= 0 {
            return self[index]
        } else {
            return nil
        }
    }
    /// <#Description#>
    ///
    /// - Parameter elements: <#elements description#>
    /// - Returns: <#return value description#>
    func appendingAll(_ elements: [Element]) -> Array {
        var tempArray = self
        for element in elements {
            tempArray = tempArray.appending(element)
        }
        return tempArray
    }
    /// <#Description#>
    ///
    /// - Parameter element: <#element description#>
    /// - Returns: <#return value description#>
    func appending(_ element: Element) -> Array {
        var tempArray = self
        tempArray.append(element)
        return tempArray
    }
    /// Places element specified location and returns array
    ///
    /// - Parameter element: Element
    /// - Returns: Array
    func inserting(_ element: Element, at index: Int) -> Array {
        var tempArray = self
        tempArray.insert(element, at: index)
        return tempArray
    }
    /// <#Description#>
    ///
    /// - Returns: <#return value description#>
    func notEmpty() -> Bool {
        return !isEmpty
    }
    /// <#Description#>
    ///
    /// - Parameter limit: <#limit description#>
    /// - Returns: <#return value description#>
    func groupBy(limit: Int)->[[Element]]{
        var sorted = self
        var groupedOptions = [[Element]]()
        for _ in 0...count{
            let first = sorted.prefix(limit)
            if !first.isEmpty{
                groupedOptions.append(first.map({$0}))
            }
            sorted = sorted.dropFirst(limit).map({$0})
        }
        return groupedOptions
    }
}
extension Array where Element == String {
    /// <#Description#>
    ///
    /// - Parameter separator: <#separator description#>
    /// - Returns: <#return value description#>
    func stringify(with separator: String)->String{
        let items = self.reduce(separator) { (collection , item ) -> String in
            
            return String(collection) + String(describing: item)+separator
            }.dropFirst()
        return String(items)
    }
    
    mutating func removeWord(item: String) {
        for word in self.enumerated() {
            if word.element == item {
                remove(at: word.offset)
            }
        }
    }
}
public func index<T: Equatable>(of element: T) -> ([T]) -> Int {
    return { array in
        
        do {
            
            if let index = try array.firstIndex(where: { $0 == element }) {
                return index
            }
            
            return 0
            
        } catch {
            return 0
        }
    }
}
