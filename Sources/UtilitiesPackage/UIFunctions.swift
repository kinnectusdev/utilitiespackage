//
//  Functions.swift
//  UIFunctions
//
//  Created by blakerogers on 9/17/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import UIKit
public typealias View = (UIView) -> UIView
public typealias SingleView = (UIView) -> Void
public typealias DoubleView = (UIView, UIView) -> Void
public typealias Label = (UILabel) -> UILabel
public typealias Button = (UIButton) -> UIButton
public typealias Layer = (CALayer) -> CALayer
public let appPurple = color(181, 168, 209)
public let appTeal = color(69,196,212)
public let appGreen = color(161,214,194)
public let appDiamond = color(230, 230, 230)
public let appSilver = color(181,181,181)
public let appGrey = color(120, 120, 120)
public let appLightShadow = color(206, 206, 206)
public let appDarkGrey = color(80, 80, 80)
public let appBlack = color(20, 20, 20)
public let appGold = color(247, 185, 69)
public let appSeaBlue = color(0, 173, 226)
public let appForestGreen = color(23, 170, 104)
public let appRoyalPurple = color(146, 81, 198)
public let appRed = color(244, 67, 54)
public let purpleRose = color(163, 181, 255, 0.57)

public let adjustableWeightAppFont: (UIFont.Weight, CGFloat) -> UIFont = { weight, size in
    //Kohinoor Devanagari", "Devanagari Sangam MN"
    return UIFont(name: "Shree Devanagari", size: size) ?? UIFont.systemFont(ofSize: size, weight: weight)
}

public let boldAppFont: (CGFloat) -> UIFont = { size in
    //Kohinoor Devanagari", "Devanagari Sangam MN"
    return UIFont(name: "Shree Devanagari", size: size) ?? UIFont.systemFont(ofSize: size, weight: .heavy)
}
public let appFontBold: (CGFloat) -> UIFont = { size in
    //Kohinoor Devanagari", "Devanagari Sangam MN"
    return UIFont(name: "Shree Devanagari", size: size) ?? UIFont.systemFont(ofSize: size, weight: .bold)
}
public let appFont: (CGFloat) -> UIFont = { size in
    //Kohinoor Devanagari", "Devanagari Sangam MN"
    return UIFont(name: "Shree Devanagari", size: size) ?? UIFont.systemFont(ofSize: size, weight: .regular)
}
public let appFontFormal: (CGFloat) -> UIFont = { size in
    //Kohinoor Devanagari", "Devanagari Sangam MN"
    return UIFont(name: "Hoefler Text", size: size) ?? UIFont.systemFont(ofSize: size, weight: .ultraLight)
}
public let whiteBoldAttributedString: (String, CGFloat) -> NSAttributedString = { text, size in
    return NSAttributedString(string: text, attributes: [.font: appFontBold(size), .foregroundColor: UIColor.white])
}
public let whiteAttributedString: (String, CGFloat) -> NSAttributedString = { text, size in
    return NSAttributedString(string: text, attributes: [.font: appFont(size), .foregroundColor: UIColor.white])
}
public let purpleAttributedString: (String, CGFloat) -> NSAttributedString = { text, size in
    return NSAttributedString(string: text, attributes: [.font: appFont(size), .foregroundColor: appPurple])
}
public let silverAttributedString: (String, CGFloat) -> NSAttributedString = { text, size in
    return NSAttributedString(string: text, attributes: [.font: appFont(size), .foregroundColor: appSilver])
}
public let greyAttributedString: (String, CGFloat) -> NSAttributedString = { text, size in
    return NSAttributedString(string: text, attributes: [.font: appFont(size), .foregroundColor: appGrey])
}
public let darkGreyAttributedString: (String, CGFloat) -> NSAttributedString = { text, size in
    return NSAttributedString(string: text, attributes: [.font: appFont(size), .foregroundColor: appDarkGrey])
}
public let formalDarkGreyAttributedString: (String, CGFloat) -> NSAttributedString = { text, size in
    return NSAttributedString(string: text, attributes: [.font: appFontFormal(size), .foregroundColor: appDarkGrey])
}
public let formalBlackAttributedString: (String, CGFloat) -> NSAttributedString = { text, size in
    return NSAttributedString(string: text, attributes: [.font: appFontFormal(size), .foregroundColor: appBlack])
}
public let blackAttributedString: (String, CGFloat) -> NSAttributedString = { text, size in
    return NSAttributedString(string: text, attributes: [.font: appFont(size), .foregroundColor: appBlack])
}
public let boldBlackAttributedString: (String, CGFloat) -> NSAttributedString = { text, size in
    return NSAttributedString(string: text, attributes: [.font: appFontBold(size), .foregroundColor: appBlack])
}
public let greenAttributedString: (String, CGFloat) -> NSAttributedString = { text, size in
    return NSAttributedString(string: text, attributes: [.font: appFont(size), .foregroundColor: appGreen])
}
public let tealAttributedString: (String, CGFloat) -> NSAttributedString = { text, size in
    return NSAttributedString(string: text, attributes: [.font: appFont(size), .foregroundColor: appTeal])
}
public let redAttributedString: (String, CGFloat) -> NSAttributedString = { text, size in
    return NSAttributedString(string: text, attributes: [.font: appFont(size), .foregroundColor: appRed])
}
public func phoneSize(_ view: UIView) -> UIView {
        view.frame = CGRect(origin: .zero, size: CGSize(width: 385, height: 667))
    return view
}
public func currentWindow() -> UIWindow {
    if let window = UIApplication.shared.delegate?.window {
        return window ?? UIWindow()
    }
    return UIWindow()
}


public func currentController() -> UIViewController {
    guard let controller = currentWindow().rootViewController else {
        assertionFailure("Must Have Root View Controller")
        return UIViewController()
    }
    if let nav = controller as? UINavigationController, let child = nav.topViewController {
        return child |> childOrControllerIfNil
    } else {
        return controller |> childOrControllerIfNil
    }
}
public func childOrControllerIfNil( controller: UIViewController) -> UIViewController {
    if let child = controller.presentedViewController {
        return child |> childOrControllerIfNil
    } else {
        return controller
    }
}
public let whiteBackground: View = { view in
    view.backgroundColor = .white
    return view
}
public let greenToBlueVerticalGradientBackground: View = { view in
    view.addGradientScreen(frame: view.frame, start: .zero, end: CGPoint(x: 0.0, y: 1.0), locations: [0.0, 0.5], colors: [appTeal.cgColor, appGreen.cgColor])
    return view
}
public let purpleToGoldVerticalGradientBackground: View = { view in
    view.addGradientScreen(frame: view.frame, start: .zero, end: CGPoint(x: 0.0, y: 1.0), locations: [0.0, 0.5], colors: [appPurple.cgColor, appGold.cgColor])
    return view
}
public let blurredWhiteBackground: View = { view in
    return view
    |> appendSubView(UIVisualEffectView(effect: UIBlurEffect(style: .extraLight))
        |> fill)
}
public let blurredDarkBackground: View = { view in
    if #available(iOS 13.0, *) {
        return view
        |> appendSubView(UIVisualEffectView(effect: UIBlurEffect(style: .systemChromeMaterialDark))
                         |> fill)
    } else {
        // Fallback on earlier versions
        return view
    }
}
public func clearBackground(view: UIView) -> UIView {
    view.backgroundColor = .clear; return view
}
public func named(_ name: String) -> View {
    return { view in
        view.name = name
        return view
    }
}
public func children(view: UIView) -> [UIView] {
    var allChildren = view.subviews
    var iterator = view.subviews.makeIterator()
    while let next = iterator.next() {
        let nextChildren = children(view: next)
        allChildren.append(contentsOf: nextChildren)
    }
    return allChildren
}

public let viewFx: (@escaping View) -> View = { fx in
    return { view in
        return fx(view)
    }
}

public func child(_ named: String) -> View {
    return { view in
        let subview = view |> children >>> filter { $0.name == named } >>> first
        guard let sub = subview else { fatalError("No child named \(named)")}
        return sub
    }
}
public func sibling(_ named: String) -> View {
    return { view in
        guard let superview = view.superview else { fatalError("No superview")}
        return superview |> child(named)
    }
}
public let silverBackground: View = backgroundColored(appSilver)
public let greyBackground: View = backgroundColored(appGrey)
public let diamondBackground: View = backgroundColored(appDiamond)
public let purpleRoseBackground: View = backgroundColored(purpleRose)
public func backgroundColored(_ color: UIColor) -> View {
    return  { view in
        view.backgroundColor = color
        return view
    }
}
public func tintColor(_ color: UIColor) -> View {
    return  { view in
        view.tintColor = color
        return view
    }
}
public func thumbTintColor(_ color: UIColor) -> View {
    return { view in
        (view as? UISlider)?.thumbTintColor = color
        return view
    }
}
public let whiteTint: View = tintColor(.white)
public func framed(_ frame: CGRect) -> View {
    return { view in
        view.frame = frame
        return view
    }
}
public let isHidden: View = { view in
    view.isHidden = true
    return view
}
public let isInvisible: View = { view in
    view.alpha = 0.0
    return view
}
public let visibility: (CGFloat) -> View = { alpha in
    return { view in
        view.alpha = alpha
        return view
    }
}
public let tagged: (Int) -> View = { tag in
    return { view in
        view.tag = tag
        return view
    }
}
public let reveal: SingleView = { view in
    view.isHidden = false
}
public let hide: SingleView = { view in
    view.isHidden = true
}
public func appendLogoIcon(view: UIView) -> UIView {
    let image = UIImageView.image(image: UIImage(named: "logo"), mode: .scaleAspectFit)
    image.frame  = CGRect(origin: .zero, size: CGSize(width: 50, height: 50))
    image.center.x = view.center.x
    image.frame.origin.y = 50
    view.addSubview(image)
    return view
}
public func color(_ red: CGFloat, _ green: CGFloat, _ blue: CGFloat, _ alpha: CGFloat = 1.0) -> UIColor {
    return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: alpha)
}
public let cornersRoundedByHalf: View  = { view in
    view.layer.cornerRadius = view.layer.frame.height*0.5
    return view
}
public func rounded(_ radius: CGFloat = 0) -> View {
    return { view in
        view.layer.cornerRadius = radius
        view.layer.masksToBounds = true
        return view
    }
}
public let bordered: (UIColor, CGFloat) -> View =  { color, width in
    return { view in
        view.layer.borderColor = color.cgColor
        view.layer.borderWidth = width
        return view
    }
}
public func roundedWithNoMask(_ radius: CGFloat = 0) -> View {
    return { view in
        view.layer.cornerRadius = radius
        return view
    }
}
/// Provides a title for a button or label
/// - Parameter title: <#title description#>
public func titled(_ title: String) -> View {
    return { view in
        if let button = view as? UIButton {
            button.setTitle(title, for: .normal)
            return button        }
        else if let label = view as? UILabel {
            label.text = title
        } else if let field = view as? UITextField {
            field.text = title
        } else {
            return view
        }
        return view
    }
}
public let placeholder: (String) -> View = { placeholderText in
    return { view in
        if let view = view as? UITextView {
            view.text = placeholderText
        } else if let view = view as? UITextField {
            view.placeholder = placeholderText
        }
        return view
    }
}
public func setTitle(_ title: String) -> SingleView {
    return { view in
        if let button = view as? UIButton {
            button.setTitle(title, for: .normal)
        } else if let label = view as? UILabel {
            label.text = title
        } else{
            
        }
    }
}
public let utilizesSendReturnType: View = { view in
    if let field = view as? UITextField {
        field.returnKeyType = .send
    } else if let textView = view as? UITextView {
        textView.returnKeyType = .send
    }
    return view
}
public let utilizesDoneReturnType: View = { view in
    if let field = view as? UITextField {
        field.returnKeyType = .done
    } else if let textView = view as? UITextView {
        textView.returnKeyType = .done
    }
    return view
}
public let utilizeNumberPad: View = { view in
    if let field = view as? UITextField {
        field.keyboardType = .numberPad
    } else if let textView = view as? UITextView {
        textView.keyboardType = .numberPad
    }
    return view
}
public func styledTitle(_ string: NSAttributedString) -> View {
    return { view in
        if let button = view as? UIButton {
            button.setAttributedTitle(string, for: .normal)
        return button
        } else if let label = view as? UILabel {
            label.attributedText = string
            return label
        } else{
            return view
        }
    }
}
public func setStyledTitle(_ string: NSAttributedString) -> SingleView {
    return { view in
        if let button = view as? UIButton {
            button.setAttributedTitle(string, for: .normal)
        } else if let label = view as? UILabel {
            label.attributedText = string
        } else{
        }
    }
}
public func setBackground(_ color: UIColor) -> SingleView {
    return { view in
        view.backgroundColor = color
    }
}
public let setImage: (UIImage?) -> View = { image in
    return { view in
        if let button = view as? UIButton {
            button.setImage(image, for: .normal)
        } else if let imageView = view as? UIImageView {
            imageView.image = image
        }
        return view
    }
}
public func roundedPurpleShadowed(_ radius: CGFloat, _ background: UIColor = .white) -> View {
    return { view in
        return view
                |> purpleShadowed
                >>> clearBackground
                >>> appendSubView(UIView()
                    |> rounded(radius)
                    >>> disableTouch
                    >>> backgroundColored(background)
                    >>> fill)
    }
}
public func roundedGreenShadowed(_ radius: CGFloat, _ background: UIColor = .white) -> View {
    return { view in
        return view
            |> greenShadowed
            >>> clearBackground
            >>> appendSubView(UIView()
                |> rounded(radius)
                >>> disableTouch
                >>> backgroundColored(background)
                >>> fill)
    }
}
public func roundedGreyShadowed(_ radius: CGFloat, _ background: UIColor = .white) -> View {
    return { view in
        return view
            |> greyShadowed
            >>> clearBackground
            >>> appendSubView(UIView()
                |> rounded(radius)
                >>> disableTouch
                >>> backgroundColored(background)
                >>> fill)
    }
}
public func roundedDiamondShadowed(_ radius: CGFloat, _ background: UIColor = .white) -> View {
    return { view in
        return view
            |> diamondShadowed
            >>> clearBackground
            >>> appendSubView(UIView()
                |> rounded(radius)
                >>> disableTouch
                >>> backgroundColored(background)
                >>> fill)
    }
}
public let disableTouch: View = { view in
    view.isUserInteractionEnabled = false
    return view
}

public let purpleShadowed: View = { view in
    view.layer.shadowColor = appPurple.cgColor
    view.layer.shadowOffset = CGSize(width: 4, height: 4)
    view.layer.shadowRadius = 4
    view.layer.shadowOpacity = 0.9
    return view
}
public let bigPurpleShadowed: View = { view in
    view.layer.shadowColor = appPurple.cgColor
    view.layer.shadowOffset = CGSize(width: 10, height: 10)
    view.layer.shadowRadius = 10
    view.layer.shadowOpacity = 0.9
    return view
}
public let bigGreenShadowed: View = { view in
    view.layer.shadowColor = appGreen.cgColor
    view.layer.shadowOffset = CGSize(width: 10, height: 10)
    view.layer.shadowRadius = 10
    view.layer.shadowOpacity = 0.9
    return view
}
public let setPurpleShadowed: SingleView = { view in
    view.layer.shadowColor = appPurple.cgColor
    view.layer.shadowOffset = CGSize(width: 4, height: 4)
    view.layer.shadowRadius = 4
    view.layer.shadowOpacity = 0.9
}
public let setGreyShadowed: SingleView = { view in
    view.layer.shadowColor = appGrey.cgColor
    view.layer.shadowOffset = CGSize(width: 4, height: 4)
    view.layer.shadowRadius = 4
    view.layer.shadowOpacity = 0.9
}
public let setTealShadowed: SingleView = { view in
    view.layer.shadowColor = appTeal.cgColor
    view.layer.shadowOffset = CGSize(width: 4, height: 4)
    view.layer.shadowRadius = 4
    view.layer.shadowOpacity = 0.9
}
public let setGreenShadowed: SingleView = { view in
    view.layer.shadowColor = appGreen.cgColor
    view.layer.shadowOffset = CGSize(width: 4, height: 4)
    view.layer.shadowRadius = 4
    view.layer.shadowOpacity = 0.9
}
public let tealShadowed: View = { view in
    view.layer.addShadow(4, dy: 4, color: appTeal, radius: 4, opacity: 0.6)
    return view
}
public let greenShadowed: View = { view in
        view.layer.addShadow(4, dy: 4, color: appGreen, radius: 4, opacity: 0.6)
        return view
    }
public let largeGreyShadowed: View = { view in
    view.layer.addShadow(0, dy: -8, color: appGrey, radius: 10, opacity: 0.6)
    return view
}
public let greyShadowed: View = { view in
    view.layer.addShadow(4, dy: 4, color: appGrey, radius: 4, opacity: 0.6)
    return view
}
public let diamondShadowed: View = { view in
    view.layer.addShadow(4, dy: 4, color: appDiamond, radius: 10, opacity: 0.6)
    return view
}
public let appendSubView: (UIView) -> View = { subview in
    return { view in
        view.add(views: subview)
        subview.constraintFx.forEach { $0(subview) }
        return view
    }
}
public let appendLayer: (CALayer) -> View = { layer in
    return { view in
        view.layer.addSublayer(layer)
        return view
    }
}
public let layerFrame: (CGRect) -> Layer = { frame in
    return { layer in
        layer.frame = frame
        return layer
    }
}
public let risingWhiteGradient: (CGSize) -> View = { size in
    return { view in
        let rect: CGRect = CGRect(origin: .zero, size: size)
        let colors = [UIColor.white, UIColor.white.withAlphaComponent(0)].map {$0.cgColor}
        return view.addingGradientScreen(frame: rect, start: CGPoint(x: 0, y: 1.0), end: .zero, locations: [0.8, 1.0], colors: colors)
    }
}
public func appendSubviews(_ subviews: [UIView]) -> View {
    return { view in
        subviews.forEach { (subview) in
            view.add(views: subview)
            subview.constraintFx.forEach { $0(subview) }
        }
        return view
    }
}
public func textForLabel(_ text: String) -> View {
    return { view in
        guard let label = view as? UILabel else { fatalError("Not a label")}
        label.text = text
        return label
    }
}

public func setTextForLabel(_ text: String) -> SingleView {
    return { view in
        guard let label = view as? UILabel else { fatalError("Not a label")}
        label.text = text
    }
}
public let textColored: (UIColor) -> View = { color in
    return { view in
        if let label = view as? UILabel {
            label.textColor = color
        } else if let field = view as? UITextField {
            field.textColor = color
        } else if let textView = view as? UITextView {
            textView.textColor = color
        }
        return view
    }
}
public let isSecure: View = { view in
    if let field = view as? UITextField {
        field.isSecureTextEntry = true
    } else if let textView = view as? UITextView {
        textView.isSecureTextEntry = true
    }
    return view
}
public let unlimitedLines: View = { view in
    guard let label = view as? UILabel else { fatalError("Not a label")}
    label.numberOfLines = 0
    return view
}
public let centerAlignedText: View = { view in
    guard let label = view as? UILabel else { fatalError("Not a label")}
    label.textAlignment = .center
    return view
}
public let textFont: (UIFont) -> View = { font in
    return { view in
        if let label = view as? UILabel {
            label.font = font
        } else if let field = view as? UITextField {
            field.font = font
        } else if let textView = view as? UITextView {
            textView.font = font
        }
        return view
    }
}
public let textRightAligned: View = textAligned(.right)
public let textLeftAligned: View = textAligned(.left)
public let textCenterAligned: View = textAligned(.center)
public let textAligned: (NSTextAlignment) -> View = { alignment in
    return { view in
        if let label = view as? UILabel {
            label.textAlignment = alignment
        } else if let field = view as? UITextField {
            field.textAlignment = alignment
        } else if let textView = view as? UITextView {
            textView.textAlignment = alignment
        }
        return view
    }
}
public func scaled(_ mode: UIView.ContentMode) -> View {
    return { view in
        guard let image = view as? UIImageView else { fatalError("Not an ImageView")}
        image.contentMode = mode
        return image
    }
}
public let scaledToFit: View = scaled(.scaleAspectFit)
public let scaledToFill: View = scaled(.scaleAspectFill)
public func isClipping(_ view: UIView) -> UIView {
    view.clipsToBounds = true
    return view
}
public let maskedToBounds: View = { view in
    view.layer.masksToBounds = true
    return view
}
public let isInterActive: View = { view in
    view.isUserInteractionEnabled = true
    return view
}
public func notClipping(_ view: UIView) -> UIView {
    view.clipsToBounds = false
    return view
}
public func pan(_ dx: CGFloat = 0, _ dy: CGFloat = 0, _ duration: TimeInterval) -> SingleView {
    return { view  in
        UIView.animate(withDuration: duration, animations:   {
            view.frame = view.frame.offsetBy(dx: dx, dy: dy)
        })
    }
}
public let scrollSize: (CGSize) -> View  = { size in
    return { view in
        guard let scrollView = view as? UIScrollView else { fatalError("Not a scroll view")}
        scrollView.contentSize = size
        return view
    }
}

public let addGesture: (_ gesture: UIGestureRecognizer) -> View = { gesture in
    return { view in
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(gesture)
        return view
    }
}
public let addTapGesture: View = { view in
    view.addGestureRecognizer(UITapGestureRecognizer())
    return view
}
public let addPanGesture: View = { view in
    view.addGestureRecognizer(UIPanGestureRecognizer())
    return view
}
public let hideScrollingControls: View = { view in
    guard let view = view as? UIScrollView else { fatalError("Not a scroll view")}
    view.showsHorizontalScrollIndicator = false
    view.showsVerticalScrollIndicator = false
    return view
}
public let utilizeConstraints: View = { view in
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
}
public let asButton: (UIView) -> UIButton = { view in
    guard let button = view as? UIButton else { fatalError("Not a button")}
    return button
}
public let asTable: (UIView) -> UITableView = { view in
    guard let table = view as? UITableView else { fatalError("Not a table")}
    return table
}
public let asImage: (UIView) -> UIImageView = { view in
    guard let image = view as? UIImageView else { fatalError("Not an image")}
    return image
}
public let contentSize: (CGFloat, CGFloat) -> View = { width, height in
    return { view in
        (view as? UIScrollView)?.contentSize = CGSize(width: width, height: height)
        return view
    }
}
public let asScrollView: (UIView) -> UIScrollView = { view in
    guard let scroll = view as? UIScrollView else { fatalError("Not a scroll view")}
    return scroll
}
public let tableWithoutSeparators: View = { view in
    guard let table = view as? UITableView else { fatalError("Not a table")}
    table.separatorStyle = .none
    return view
}
public let tableDelegate: (UITableViewDelegate) -> View = { delegate in
    return { view in
        guard let table = view as? UITableView else { fatalError("Not a table")}
        table.delegate = delegate
        return view
    }
}
public let tableDataSource: (UITableViewDataSource) -> View = { datasource in
    return { view in
        guard let table = view as? UITableView else { fatalError("Not a table")}
        table.dataSource = datasource
        return view
    }
}
public let listAllowsSelection: (Bool) -> View = { allowsSelection in
    return { view in
        (view as? UITableView)?.allowsSelection = allowsSelection
        (view as? UICollectionView)?.allowsSelection = allowsSelection
        return view
    }
}
public let registerTableCell: (AnyClass?, String) -> View = { cellType, identifier in
    return { view in
        guard let table = view as? UITableView else { fatalError("Not a table")}
        table.register(cellType, forCellReuseIdentifier: identifier)
        return view
    }
}
public let cellHeight: (CGFloat) -> View = { height in
    return { view in
        guard let table = view as? UITableView else { fatalError("Not a table")}
        table.rowHeight = height
        return view
    }
}
public let scrollDisabled: View = { view in
    guard let scrollView = view as? UIScrollView else { fatalError("Not a scroll view")}
    scrollView.isScrollEnabled = false
    return view
}
public let appendArrangedSubviews: ([UIView]) -> View = { views in
    return { view in
        guard let view = view as? UIStackView else { fatalError("Not a stack view")}
        views.forEach {
            view.addArrangedSubview($0)
        }
        return view
    }
}
public let centerAlignStackView: View = { view in
    guard let view = view as? UIStackView else { fatalError("Not a stack view")}
    view.alignment = UIStackView.Alignment.center
    return view
}
public let equalDistributionStackView: View = { view in
    guard let view = view as? UIStackView else { fatalError("Not a stack view")}
    view.distribution = UIStackView.Distribution.equalSpacing
    return view
}
public let fillProportionallyStackView: View = { view in
    guard let view = view as? UIStackView else { fatalError("Not a stack view")}
    view.distribution = UIStackView.Distribution.fillProportionally
    return view
}
public let horizontalStackView: View = { view in
    guard let view = view as? UIStackView else { fatalError("Not a stack view")}
    view.axis = .horizontal
    return view
}
public let verticalStackView: View = { view in
    guard let view = view as? UIStackView else { fatalError("Not a stack view")}
    view.axis = .vertical
    return view
}
public func stackSubviewsVertically(_ subviews: [UIView], _ dx: CGFloat = 8, _ initialOffset: CGFloat = 0) -> View {
    return { view in
       let stack = UIStackView(arrangedSubviews: subviews)
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.alignment = .fill
        stack.axis = .vertical
        stack.distribution = .equalSpacing
        view.add(views: stack)
        stack.constrainInView(view: view, top: 0, left: dx, right: -dx, bottom: 0)
        return view
    }
}
public func appendSubviewsHorizontally(_ subviews: [UIView], _ dx: CGFloat = 8, _ initialOffset: CGFloat = 0) -> View {
    return { view in
        subviews.enumerated().forEach({ (offset, element) in
            element.translatesAutoresizingMaskIntoConstraints = false
            if let previous = subviews.itemAt(offset-1) {
                view.addSubview(element)
                element.constrainLeftToRight(of: previous, constant: dx)
                element.constrainCenterYTo(view: previous, constant: 0)
            } else {
                view.addSubview(element)
                element.constrainLeftToLeft(of: view, constant: initialOffset)
                element.constrainCenterYTo(view: view, constant: 0)
            }
        })
        return view
    }
}
public func alignSubviewsToCenterX(_ subviews: [UIView]) -> View {
    return { view in
        subviews.enumerated().forEach({ (offset, element) in
            element.translatesAutoresizingMaskIntoConstraints = false
            if !element.isDescendant(of: view) {
                view.addSubview(element)
            }
            element.constrainCenterXTo(view: view, constant: 0)
        })
        return view
    }
}
public func alignSubviewsToTop(_ subviews: [UIView], _ dy: CGFloat = 8) -> View {
    return { view in
        subviews.enumerated().forEach({ (offset, element) in
            element.translatesAutoresizingMaskIntoConstraints = false
            if !element.isDescendant(of: view) {
                view.addSubview(element)
            }
            element.constrainTopToTop(of: view, constant: dy)
        })
        return view
    }
}
public func alignSubviewsToBottom(_ subviews: [UIView], _ dy: CGFloat = 8) -> View {
    return { view in
        subviews.enumerated().forEach({ (offset, element) in
            element.translatesAutoresizingMaskIntoConstraints = false
            if !element.isDescendant(of: view) {
                view.addSubview(element)
            }
            element.constrainBottomToBottom(of: view, constant: dy)
        })
        return view
    }
}
public func alignSubviewsLeading(_ subviews: [UIView], _ dx: CGFloat = 8) -> View {
    return { view in
        subviews.enumerated().forEach({ (offset, element) in
            element.translatesAutoresizingMaskIntoConstraints = false
            if !element.isDescendant(of: view) {
                view.addSubview(element)
            }
            element.constrainLeftToLeft(of: view, constant: dx)
        })
        return view
    }
}
public func appendSubviewsVertically(_ subviews: [UIView], _ dy: CGFloat = 8, _ initialOffset: CGFloat = 0) -> View {
    return { view in
        subviews.enumerated().forEach({ (offset, element) in
            element.translatesAutoresizingMaskIntoConstraints = false
            if let previous = subviews.itemAt(offset-1) {
                if !element.isDescendant(of: view) {
                    view.addSubview(element)
                }
                element.constrainTopToBottom(of: previous, constant: dy)
            } else {
                if !element.isDescendant(of: view) {
                    view.addSubview(element)
                }
                element.constrainTopToTop(of: view, constant: initialOffset)
            }
        })
        return view
    }
}
public let asSwitch: (UIView) -> UISwitch = { view in
    guard let uiswitch = view as? UISwitch else { fatalError("Not a switch")}
    return uiswitch
}
public let asLabel: (UIView) -> UILabel = { view in
    guard let label = view as? UILabel else { fatalError("Not a label")}
    return label
}
public let asField: (UIView) -> UITextField = { view in
    guard let field = view as? UITextField else { fatalError("Not a text field")}
    return field
}
public let asTextView: (UIView) -> UITextView = { view in
    guard let textView = view as? UITextView else { fatalError("Not a textView")}
    return textView
}
public let asCollection: (UIView) -> UICollectionView = { view in
    guard let view = view as? UICollectionView else { fatalError("Not a testView")}
    return view
}
public let isPaging: View = { view in
    if let view = view as? UIScrollView {
        view.isPagingEnabled = true
    }
    return view
}
public let registerCell: (AnyClass?, String) -> View = { (itemType, identifier) in
    return { view in
        if let view = view as? UITableView {
            view.register(itemType, forCellReuseIdentifier: identifier)
            return view
        } else if let view = view as? UICollectionView {
            view.register(itemType, forCellWithReuseIdentifier: identifier)
            return view
        } else {
            return view
        }
    }
}
public let collectionDelegate: (UICollectionViewDelegate) -> View = { delegate in
    return { view in
        guard let view = view as? UICollectionView else { fatalError("Not a collection view")}
        view.delegate = delegate
        return view
    }
}
public let collectionDatasource: (UICollectionViewDataSource) -> View = { datasource in
    return { view in
        guard let view = view as? UICollectionView else { fatalError("Not a collection view")}
        view.dataSource = datasource
        return view
    }
}
public let contentInset: (UIEdgeInsets) -> View = { edgeInset in
    return { view in
        guard let view = view as? UIScrollView else { fatalError("Not a scroll view")}
        view.contentInset = edgeInset
        return view
    }
}
public let switchColor: (UIColor) -> View = { color in
    return { view in
        guard let view = view as? UISwitch else { fatalError("Not a switch")}
        view.onTintColor = color
        return view
    }
}
public let unlimitedRect: ([CGRect]) -> CGRect = { frames in
    return frames.reduce(CGRect.zero, { accumRect, frame -> CGRect in
        let screenWidth = UIScreen.main.bounds.width
        if frame.debugDescription == frames.first?.debugDescription ?? "" {
            return accumRect
                        |> prop(\.size.height)({ height in
                            return height + frame.height
                        })
                        |> prop(\.size.width)({ width in
                            return frame.width
                        })
        } else if frame.debugDescription == frames.last?.debugDescription ?? "" {
            let willPassBoundary = accumRect.width + frame.width > screenWidth
            let finalFramePlusHeight = accumRect
                                    |> prop(\.size.height)({ height in
                                        return height + frame.height
                                    })
                                    |> prop(\.size.width)({ width in
                                        return screenWidth
                                    })
            let finalFrame = accumRect
                                    |> prop(\.size.width)({ width in
                                        return screenWidth
                                    })
            return willPassBoundary ? finalFramePlusHeight : finalFrame
        } else {
            let willPassBoundary = accumRect.width + frame.width > screenWidth
            let framePlusHeight = accumRect
                                    |> prop(\.size.height)({ height in
                                        return height + frame.height
                                    })
                                    |> prop(\.size.width)({ width in
                                        return 0
                                    })
            let framePlusWidth = accumRect
                                    |> prop(\.size.width)({ width in
                                        return width + frame.width
                                    })
            return willPassBoundary ? framePlusHeight : framePlusWidth
        }
    })
}
public func adjustHeight(dy: CGFloat) -> (CGRect) -> CGRect {
    return { rect in
        return CGRect(x: rect.minX, y: rect.minY, width: rect.width, height: rect.height + dy)
    }
}

public func adjustWidth(dx: CGFloat) -> (CGRect) -> CGRect {
    return { rect in
        return CGRect(x: rect.minX, y: rect.minY, width: rect.width + dx, height: rect.height)
    }
}
public func multiplyHeight(dy: CGFloat) -> (CGRect) -> CGRect {
    return { rect in
        return CGRect(x: rect.minX, y: rect.minY, width: rect.width, height: rect.height * dy)
    }
}

public func multiplyWidth(dx: CGFloat) -> (CGRect) -> CGRect {
    return { rect in
        return CGRect(x: rect.minX, y: rect.minY, width: rect.width * dx, height: rect.height)
    }
}
public let rectSize: (CGRect) -> CGSize = { rect in
    rect.size
}
public func adjustHeight(dy: Double) -> (CGSize) -> CGSize {
    return { size in
        return CGSize(width: size.width, height: size.height + dy)
    }
}

public func adjustWidth(dx: Double) -> (CGSize) -> CGSize {
    return { size in
        return CGSize(width: size.width + dx, height: size.height)
    }
}
public func multiplyHeight(dy: Double) -> (CGSize) -> CGSize {
    return { size in
        return CGSize(width: size.width, height: size.height * dy)
    }
}

public func multiplyWidth(dx: Double) -> (CGSize) -> CGSize {
    return { size in
        return CGSize(width: size.width * dx, height: size.height)
    }
}
