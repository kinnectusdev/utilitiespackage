//
//  Number_Extensions.swift
//  Utilities
//
//  Created by blakerogers on 3/20/19.
//  Copyright © 2019 blakerogers. All rights reserved.
//

import Foundation
import UIKit
public extension CGSize {
    static func + (lhs: CGSize, rhs: CGSize) -> CGSize {
        return CGSize(width: lhs.width + rhs.width, height: lhs.height + rhs.height)
    }
    static func += (lhs: CGSize, rhs: CGSize) -> CGSize {
        return CGSize(width: lhs.width + rhs.width, height: lhs.height + rhs.height)
    }
}
public extension Int {
    func float() -> Float {
        return Float(self)
    }
    func cgFloat() -> CGFloat {
        return CGFloat(self)
    }
    func double() -> Double {
        return Double(self)
    }
}
public let asCGFloat: (Any?) -> CGFloat = { number in
    if let number = number as? Int {
        return CGFloat(number)
    } else if let number = number as? Double {
        return CGFloat(number)
    }
    assertionFailure("Not a valid type")
    return 0
}
public extension CGFloat {
    /// Evaluates whether a value is within a range
    ///
    /// - Parameters:
    ///   - value: CGFloat (Center value)
    ///   - offset: CGFloat
    /// - Returns: Bool
    func isApproximately(_ value: CGFloat, _ offset: CGFloat) -> Bool {
        return (value-offset...value+offset).contains(self)
    }
}

public let number: (Double) -> NSNumber = { double in
    NSNumber(value: double)
}
