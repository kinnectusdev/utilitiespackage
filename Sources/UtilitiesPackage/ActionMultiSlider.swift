//
//  ActionMultiSlider.swift
//  Meld
//
//  Created by Blake Rogers on 4/27/22.
//  Copyright © 2022 com.meld.kinnectus. All rights reserved.
//

import MultiSlider
import UIKit

class ActionMultiSlider: MultiSlider {
    typealias ValueChanged = () -> Void
    typealias DidTouch = () -> Void
    private var valueChanged: ValueChanged!
    private var didTouch: DidTouch!
    @objc func valueChangedHandler(_ sender: MultiSlider) {
        valueChanged()
    }
    @objc func didTouchHandler(_ sender: MultiSlider) {
       didTouch()
    }
    func setValueChanged(_ valueChanged: @escaping ValueChanged) {
        self.valueChanged = valueChanged
        self.addTarget(self, action: #selector(valueChangedHandler(_:)), for: .valueChanged)
    }
                       
    func setDidTouch(_ didTouch: @escaping DidTouch) {
       self.didTouch = didTouch
       self.addTarget(self, action: #selector(didTouchHandler(_:)), for: .touchUpInside)
    }
}
