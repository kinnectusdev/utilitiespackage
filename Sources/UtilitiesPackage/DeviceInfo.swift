//
//  DeviceInfo.swift
//  UIFunctions
//
//  Created by blakerogers on 10/25/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UIKit
public enum DeviceType {
    case iPhone5_5S_5C
    case iPhone6_6S_7_8
    case iPhone6_6S_7_8Plus
    case iPhoneX
    case iPhoneXSMax
    case iPhoneXR
    case iPad
    case tv
    case car
    case unknown
    public var hasNotch: Bool {
        switch self {
        case .iPhoneX, .iPhoneXR, .iPhoneXSMax: return true
        default: return false
        }
    }
}
public struct DeviceInfo {
    public static var deviceType: DeviceType {
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            switch UIScreen.main.nativeBounds.height {
            case 1136:
               // "iPhone 5 or 5S or 5C"
                return .iPhone5_5S_5C
            case 1334:
                //"iPhone 6/6S/7/8"
                return .iPhone6_6S_7_8
            case 2208:
                //"iPhone 6+/6S+/7+/8+"
                return .iPhone6_6S_7_8Plus
            case 2436:
                //"iPhone X"
                return .iPhoneX
            case 2688:
                //"iPhone XSMax"
                return .iPhoneXSMax
            case 1792:
                //"iPhone XR"
                return .iPhoneXR
            default:
                //"unknown"
                return .unknown
            }
        case .pad:
            return .iPad
        case .tv:
            return .tv
        case .carPlay:
            return .car
        case .unspecified:
            return .unknown
        case .mac:
            return .unknown
        @unknown default:
            return .unknown
        }
    }
}
