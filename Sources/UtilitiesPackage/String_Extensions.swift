//
//  String_Extensions.swift
//  Utilities
//
//  Created by blakerogers on 3/20/19.
//  Copyright © 2019 blakerogers. All rights reserved.
//

import UIKit
public extension String {
    func notEmpty() -> Bool {
        return !self.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
    }
}
public extension String {
    /// <#Description#>
    ///
    /// - Parameters:
    ///   - width: <#width description#>
    ///   - textSize: <#textSize description#>
    /// - Returns: <#return value description#>
    func rectForText(width: CGFloat, textSize: CGFloat)->CGRect{
        let size = CGSize(width: width, height: 1000)
        
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let estimateRect = NSString(string: self).boundingRect(with: size, options: options, attributes: convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.font):UIFont.systemFont(ofSize: textSize)]), context: nil)
        return estimateRect
    }
    
    /// <#Description#>
    ///
    /// - Parameters:
    ///   - strings: <#strings description#>
    ///   - words: <#words description#>
    /// - Returns: <#return value description#>
    func replace(strings:[String], with words: [String])->String{
        var newWord = self
        for (index,string) in strings.enumerated() {
            newWord = newWord.replacingOccurrences(of: string , with: words[index])
        }
        return newWord
    }
    /// <#Description#>
    ///
    /// - Returns: <#return value description#>
    func array()->[String]{
        return self.split(separator: ",").compactMap({String($0)})
    }
    /// <#Description#>
    ///
    /// - Returns: <#return value description#>
    func possessiveForm() -> String {
        if let lastLetter = self.array().last {
            if lastLetter.caseInsensitiveCompare("s") == .orderedSame {
                return self+"'"
            } else {
                return self+"'s"
            }
        }
        return self
    }
    ///Create a string that returns a properly formatted sentence with the first letter of each word capitalize and subsequent letters lower cased
    func formalString() -> String {
        let split = self.lowercased().split(separator: " ").map({String(describing: $0)})
        return split.compactMap({ return $0.prefix(1).uppercased() + $0.dropFirst()}).joined(separator: " ")
    }
}
// Helper public function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
    guard let input = input else { return nil }
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
// Helper public function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
    return input.rawValue
}
