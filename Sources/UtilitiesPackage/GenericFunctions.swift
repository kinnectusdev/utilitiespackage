//
//  GenericFunctions.swift
//  Meld
//
//  Created by blakerogers on 11/20/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import Foundation
struct GenericError: Error {
    let error: String
}
public let stringValueFromSequence: (String) -> ([String: Any]) -> String = { key in
    return { dictionary in
       let value = dictionary[key]
        if let value = value as? NSString {
            return String(value)
        }
        return (value as? String) ?? ""
    }
}
public let stringValuesFromSequence: (String) -> ([String: Any]) -> [String] = { key in
    return { dictionary in
       let value = dictionary[key]
        if let values = value as? [NSString] {
            return values.map { String($0)}
        }
        return (value as? [String]) ?? [""]
    }
}
public let asMutableString: (Any) -> String = { value in
    guard let value = value as? NSString else { return "" }
    return value.appending("")
}
public let asString: (Any) -> String = { value in
    guard let value = value as? String else { return "" }
    return value
}
public let asNumber: (Any) -> NSNumber = { value in
    guard let value = value as? NSNumber else { return NSNumber(value: 0) }
    return value
}
public let asInteger: (Any) -> Int = { value in
    guard let value = value as? Int else { return 0 }
    return value
}
public let asDouble: (Any) -> Double = { value in
    guard let value = value as? Double else { return 0 }
    return value
}

public let asBoolean: (Any) -> Bool = { value in
    guard let value = value as? Bool else { return true }
    return value
}
public let asStringSequence: (Any) -> [String] = { value in
    guard let value = value as? [String] else { return [""] }
    return value
}
public let asStringToStringDictionarySequence: (Any) -> [[String: String]] = { value in
    guard let value = value as? [[String: String]] else { return [] }
    return value
}
public let asStringToStringArrayDictionarySequence: (Any) -> [String: [String]] = { value in
    guard let value = value as? [String: [String]] else { return [:] }
    return value
}
public let asStringIntDictionarySequence: (Any) -> [String: Int] = { value in
    guard let value = value as? [String: Int] else { return [:] }
    return value
}
public func flatten<A>( _ doubleArray: [[A]]) -> [A] {
    return doubleArray.reduce([], { $0.appendingAll($1)})
}
public func otherwise<E: Any>(_ backup: E)  -> (Optional<E>) -> E {
    return { optional in
        switch optional {
        case .none: return backup
        case .some(let value): return value
        }
    }
}
public func prepending<A>(_ value: A) -> ([A]) -> [A] {
    return { array in
        [value] |> appendingAll(array)
    }
}
public func appending<A>( _ value: A) -> ([A]) -> [A] {
    return { array in
        return array.appending(value)
    }
}
public func appendingAll<A>( _ values: [A]) -> ([A]) -> [A] {
    return { array in
        return array.appendingAll(values)
    }
}
public func removing<A: Equatable>( _ value: A) -> ([A]) -> [A] {
    return { array in
        return array.removing(element: value)
    }
}
public func removingWhere<A>( _ evaluation: @escaping (A) -> Bool) -> ([A]) -> [A] {
    return { array in
        return array.filter { !evaluation($0)}
    }
}
public func randomSelection<T>(sequence: [T]) -> T {
    return sequence[(Int.random(in: 0..<sequence.count))]
}
public func first<T>(sequence: [T]) -> T? {
    return sequence.first
}
public func second<T>(sequence: [T]) -> T? {
    return sequence.itemAt(1)
}
public func third<T>(sequence: [T]) -> T? {
    return sequence.itemAt(2)
}
public func fourth<T>(sequence: [T]) -> T? {
    return sequence.itemAt(3)
}
public func fifth<T>(sequence: [T]) -> T? {
    return sequence.itemAt(4)
}
public func sixth<T>(sequence: [T]) -> T? {
    return sequence.itemAt(5)
}
public func last<T>(sequence: [T]) -> T? {
    return sequence.last
}
public func firstFour<T>(sequence: [T]) -> [T] {
    return sequence.groupBy(limit: 4).first ?? []
}
public func tupleFourFromArray<T>(sequence: [T]) ->  (T,T,T,T) {
    assert(sequence.count >= 4, "Four values not in sequence")
    return (sequence[0], sequence[1], sequence[2], sequence[3])
}
public func uniqueElements<T: Hashable>(_ sequence: [T]) -> [T] {
    return Set(sequence).compactMap { $0 }
}
public let hasQuestionPrefix: (String) -> Bool = { text in
    return ["Who", "What", "When", "Where", "How", "Do", "Is"].map { text.description.hasPrefix($0)}.contains(true)
  
}
