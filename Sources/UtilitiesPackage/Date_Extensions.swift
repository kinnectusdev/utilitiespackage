//
//  Date_Extensions.swift
//  Utilities
//
//  Created by blakerogers on 3/24/19.
//  Copyright © 2019 blakerogers. All rights reserved.
//

import Foundation
public extension TimeInterval {
    /// Formatted date
    /// M/D
    /// - Returns: String
    func shortDate() -> String {
        let components = Calendar.current.dateComponents([.month, .day], from: Date(timeIntervalSince1970: self))
        guard let month = components.month, let day = components.day else { return ""}
        return "\(month)/\(day)"
    }
    func day_time() -> String {
        let components = Calendar.current.dateComponents([.month, .day, .hour, .minute], from: Date(timeIntervalSince1970: self))
        guard let month = components.month, let day = components.day, let hour = components.hour, let minute = components.minute else { return ""}
        let time = hour > 12 ? "\(hour-12):\(minute) PM" : "\(hour):\(minute) AM"
        return "\(month).\(day) \(time)"
    }
    func chatTimeStamp() -> String {
        let todayComponents = Calendar.current.dateComponents([.month, .day, .hour, .minute], from: Date())
        let components = Calendar.current.dateComponents([.month, .day, .hour, .minute], from: Date(timeIntervalSince1970: self))
        guard let month = components.month, let day = components.day, let hour = components.hour, let minute = components.minute else { return ""}
        return "\(month)/\(day) \(hour):\(minute)"
    }
}
