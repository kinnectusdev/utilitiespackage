//
//  String + Utils.swift
//  Meld
//
//  Created by Blake Rogers on 5/1/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation

public extension String {
    static let empty = ""
}
