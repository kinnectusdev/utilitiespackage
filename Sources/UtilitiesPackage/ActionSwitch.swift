//
//  ActionSwitch.swift
//  Meld
//
//  Created by Blake Rogers on 4/27/22.
//  Copyright © 2022 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UIKit


class ActionSwitch: UISwitch {
    typealias Action = () -> Void
     
    private var action: Action!

    @objc func handleAction(_ sender: UIButton) {
        action()
    }
    
    func setAction(_ action: @escaping Action) {
         self.action = action
         self.addTarget(self, action: #selector(handleAction(_:)), for: .touchUpInside)
     }
}
