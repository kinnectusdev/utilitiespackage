//
//  File.swift
//  
//
//  Created by Blake Rogers on 4/29/22.
//

import Foundation
import UIKit

class ActionButton: UIButton {
    typealias Action = () -> Void
     
    private var action: Action!

    @objc func handleAction(_ sender: UIButton) {
        action()
    }
    
    func setAction(_ action: @escaping Action) {
         self.action = action
         self.addTarget(self, action: #selector(handleAction(_:)), for: .touchUpInside)
     }
}
extension ActionButton {
    convenience init(action: Action?) {
        self.init(frame: CGRect.zero)
        if let action = action {
            setAction(action)
        }
    }
}

