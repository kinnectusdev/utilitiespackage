//
//  Utilities.swift
//  Utilities
//
//  Created by blakerogers on 3/6/19.
//  Copyright © 2019 blakerogers. All rights reserved.
//
import Foundation
import AVFoundation
import Darwin
import MapKit

public class Utilities {
   
    static func makeFullName(from name: String) -> (String, String) {
        let firstLast = name.split(separator: " ").map { String($0).trimmingCharacters(in: .whitespaces)}
        guard firstLast.count > 1 else { return (name, "") }
        guard let firstName = firstLast.first, let lastName = firstLast.last else { return (name, "") }
        return (firstName, lastName)
    }
    static func formattedName(_ name: String) -> (firstName: String, lastName: String) {
        let first_name = name.split(separator: " ").first ?? ""
        let last_name = name.split(separator: " ").last ?? ""
        return (String(first_name),String(last_name))
    }
    static func testCLCoordinates() -> CLLocationCoordinate2D {
        CLLocationCoordinate2D(latitude: testCoordinates()[0].0, longitude: testCoordinates()[0].1)
    }
    static func testCoordinates() -> [(Double,Double)] {
        let coord1 = (36.8777412,-76.3033785)
        let coord2 = (36.8839664,-76.3039779)
        let coord3 = (36.8494557,-76.2893914)
        return [coord1, coord2, coord3]
    }
    static func distanceInMiles(distance: Double) -> String {
        let metersPerMile: Double = 1600
        let calculation: Double = distance/metersPerMile
        let value = NSNumber(value: calculation)
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.roundingMode = .up
        formatter.maximumFractionDigits = 2
        return (formatter.string(from: value) ?? "0") + " mi"
    }
    static func timeFor(time: CMTime) -> String {
        let seconds = NSNumber(value: round(time.seconds)).intValue
        var minutes = seconds % 60
        minutes = round(time.seconds) < 60 ? 0 : minutes
        let zero = seconds < 10 ? "0" : ""
        let secondString = zero + String(describing: seconds - minutes*60)
        let minuteString = String(describing: minutes)
        return "\(minuteString):\(secondString)"
    }
    static func progressFor(time: CMTime, duration: CMTime) -> Float {
        return 0
        ///TODO: Reduce Build Time
        return Float(time.seconds/duration.seconds)
    }
    static func coordinateRange(center: Double, delta: Double) -> (min: Double, max: Double) {
        let maxLimit = max(center+delta, center-delta)
        let minLimit = min(center+delta, center-delta)
        return (minLimit, maxLimit)
    }
}
extension Utilities {
    /// <#Description#>
    ///
    /// - Parameters:
    ///   - array: <#array description#>
    ///   - item: <#item description#>
    /// - Returns: <#return value description#>
    static public func appendIfNil(array: inout [String]?, item: String) -> [String]? {
        if array != nil {
            array?.append(item)
            return array
        } else {
            return [item]
        }
    }
}
public func onMainThread( _ action: @escaping () -> Void) {
    DispatchQueue.main.async {
        action()
    }
}
