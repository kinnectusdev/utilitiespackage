//
//  IconFont.swift
//  Meld
//
//  Created by Blake Rogers on 8/30/21.
//  Copyright © 2021 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UIKit

///Back Icon
public func backFontIcon(size: CGFloat, color: UIColor = .white) -> NSAttributedString {
    return NSAttributedString(string: 0xf101 |> stringForIcon, attributes: [.font: UIFont(name: "flaticon", size: size) as Any, .foregroundColor: color as Any])
}
///Heart Icon
public func heartFontIcon(size: CGFloat, color: UIColor = .white) -> NSAttributedString {
    return NSAttributedString(string: 0xf102 |> stringForIcon, attributes: [.font: UIFont(name: "flaticon", size: size) as Any, .foregroundColor: color as Any])
}
///Share Icon
public func shareFontIcon(size: CGFloat, color: UIColor = .white) -> NSAttributedString {
    return NSAttributedString(string: 0xf103 |> stringForIcon, attributes: [.font: UIFont(name: "flaticon", size: size) as Any, .foregroundColor: color as Any])
}
//////Add Icon
public func addFontIcon(size: CGFloat, color: UIColor = .white) -> NSAttributedString {
    return NSAttributedString(string: 0xf104 |> stringForIcon, attributes: [.font: UIFont(name: "flaticon", size: size) as Any, .foregroundColor: color as Any])
}
//////Dismiss Icon
public func dismissFontIcon(size: CGFloat, color: UIColor = .white) -> NSAttributedString {
    return NSAttributedString(string: 0xf105 |> stringForIcon, attributes: [.font: UIFont(name: "flaticon", size: size) as Any, .foregroundColor: color as Any])
}
//////Delete Icon
public func deleteFontIcon(size: CGFloat, color: UIColor = .white) -> NSAttributedString {
    return NSAttributedString(string: 0xf106 |> stringForIcon, attributes: [.font: UIFont(name: "flaticon", size: size) as Any, .foregroundColor: color as Any])
}
///Next Icon
public func nextFontIcon(size: CGFloat, color: UIColor = .white) -> NSAttributedString {
    return NSAttributedString(string: 0xf107 |> stringForIcon, attributes: [.font: UIFont(name: "flaticon", size: size) as Any, .foregroundColor: color as Any])
}
//////Phone Icon
public func phoneFontIcon(size: CGFloat, color: UIColor = .white) -> NSAttributedString {
    return NSAttributedString(string: 0xf108 |> stringForIcon, attributes: [.font: UIFont(name: "flaticon", size: size) as Any, .foregroundColor: color as Any])
}
///VideoCall Icon
public func videoFontIcon(size: CGFloat, color: UIColor = .white) -> NSAttributedString {
    return NSAttributedString(string: 0xf109 |> stringForIcon, attributes: [.font: UIFont(name: "flaticon", size: size) as Any, .foregroundColor: color as Any])
}
///Microphone Icon
public func microPhoneFontIcon(size: CGFloat, color: UIColor = .white) -> NSAttributedString {
    return NSAttributedString(string: 0xf10a |> stringForIcon, attributes: [.font: UIFont(name: "flaticon", size: size) as Any, .foregroundColor: color as Any])
}
///Conversation Icon
public func conversationFontIcon(size: CGFloat, color: UIColor = .white) -> NSAttributedString {
    return NSAttributedString(string: 0xf10b |> stringForIcon, attributes: [.font: UIFont(name: "flaticon", size: size) as Any, .foregroundColor: color as Any])
}
///Message Icon
public func messageFontIcon(size: CGFloat, color: UIColor = .white) -> NSAttributedString {
    return NSAttributedString(string: 0xf10c |> stringForIcon, attributes: [.font: UIFont(name: "flaticon", size: size) as Any, .foregroundColor: color as Any])
}
///Account Icon
public func accountFontIcon(size: CGFloat, color: UIColor = .white) -> NSAttributedString {
    return NSAttributedString(string: 0xf10d |> stringForIcon, attributes: [.font: UIFont(name: "flaticon", size: size) as Any, .foregroundColor: color as Any])
}

///Filter Icon
public func filterFontIcon(size: CGFloat, color: UIColor = .white) -> NSAttributedString {
    return NSAttributedString(string: 0xf10e |> stringForIcon, attributes: [.font: UIFont(name: "flaticon", size: size) as Any, .foregroundColor: color as Any])
}

public func image(size: CGFloat, dx: CGFloat = 0, dy: CGFloat = 0) -> (String) -> UIImage {
    return { iconText in
        guard let font = UIFont(name: "flaticon", size: size) else { return UIImage()}
        let symbol = NSAttributedString(string: iconText, attributes: [.font: font])
        let mutableSymbol = NSMutableAttributedString(attributedString: symbol)
        let rect = CGRect(x: dx, y: dy, width: size, height: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        mutableSymbol.draw(in: rect.applying(CGAffineTransform(scaleX: 2.0, y: 2.0)))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image?.withRenderingMode(.alwaysTemplate) ?? UIImage()
    }
}
public let stringForIcon: (UInt32) -> String = { rawIcon in
    var icon = rawIcon
    let xPtr = withUnsafeMutablePointer(to: &icon, { $0 })
    return String(bytesNoCopy: xPtr, length:MemoryLayout<UInt32>.size, encoding: String.Encoding.utf32LittleEndian, freeWhenDone: false) ?? ""
}
