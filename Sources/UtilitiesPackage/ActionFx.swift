//
//  ActionFx.swift
//  UIFunctions
//
//  Created by blakerogers on 9/19/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import UIKit
public typealias ViewAction = (UIView) -> Void
///TODO: Reduce Build Time
public let onTap: (@escaping ViewAction) -> View = { action in
    return { view in
            view.isUserInteractionEnabled = true
            view.addTapGestureRecognizer {
                action(view)
            }
        return view
    }
}
