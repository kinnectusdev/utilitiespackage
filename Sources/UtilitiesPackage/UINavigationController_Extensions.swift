//
//  UINavigationController_Extensions.swift
//  Utilities
//
//  Created by blakerogers on 3/20/19.
//  Copyright © 2019 blakerogers. All rights reserved.
//

import UIKit
public extension UINavigationController {
    open override var prefersStatusBarHidden: Bool {
        return topViewController?.prefersStatusBarHidden ?? false
    }
    override open var childForStatusBarHidden: UIViewController? {
        return topViewController
    }
    /// Overriding the preferredStatusBarStyle variable for the UINavigationControllers so that the status bar loads pertinent to the viewcontroller loaded in the navigation controller
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return topViewController?.preferredStatusBarStyle ?? .default
    }
    open override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return topViewController?.preferredStatusBarUpdateAnimation ?? .fade
    }
    /// Return a navigation controller with a pushed child on stack
    ///
    /// - Parameter controller: UIViewController
    /// - Returns: UINavigationController
    func pushing(controller: UIViewController) -> UINavigationController {
        self.pushViewController(controller, animated: false)
        return self
    }
    /// Return navigation controller with bar enabled or disabled
    ///
    /// - Parameter enabled: Bool
    /// - Returns: UINavigationController
    func enablingNavigationBar(_ enabled: Bool) -> UINavigationController {
        navigationBar.isHidden = !enabled
        return self
    }
}
