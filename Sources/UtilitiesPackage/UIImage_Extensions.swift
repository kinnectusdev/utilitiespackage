//
//  UIImage_Extensions.swift
//  Utilities
//
//  Created by blakerogers on 3/20/19.
//  Copyright © 2019 blakerogers. All rights reserved.
//

import UIKit

/// <#Description#>
///
/// - Parameters:
///   - image: <#image description#>
///   - size: <#size description#>
/// - Returns: <#return value description#>
public func scaleImageToSize(image: UIImage,size:CGSize)->UIImage{
    UIGraphicsBeginImageContext(size)
    let rect = CGRect(origin: CGPoint.zero, size: size)
    image.draw(in: rect)
    let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return scaledImage!
}

public func imageWith(newSize: CGSize) -> (UIImage) -> UIImage {
    return { image in
        if #available(iOS 10.0, *) {
            let newImage = UIGraphicsImageRenderer(size: newSize).image { _ in
                image.draw(in: CGRect(origin: .zero, size: newSize))
            }
            return newImage.withRenderingMode(image.renderingMode)

        }
        return image
    }
}
