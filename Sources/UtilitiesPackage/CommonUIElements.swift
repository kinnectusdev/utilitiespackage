//
//  CommonUIElements.swift
//  MeldAppProject
//
//  Created by blakerogers on 11/10/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import Foundation
import UIKit

public let horizontalScrollView: ([UIView], CGFloat ) -> UIView = { subviews, height in
    let accumulativeWidth = subviews |> map { $0.frame.width } |> reduce(0, { $0 + $1 })
    return UIScrollView()
            |> hideScrollingControls
            >>> scrollSize(CGSize(width: accumulativeWidth, height: height))
            >>> appendSubView(UIView()
            |> clearBackground
            >>> framed(CGRect(origin: .zero, size: CGSize(width: accumulativeWidth, height: height)))
            >>> appendSubviewsHorizontally(subviews))
}
public let titleLabel: (String) -> UIView = { text in
    return UILabel()
        |> textAligned(.center)
        >>> named("title")
        >>> unlimitedLines
        >>> styledTitle(darkGreyAttributedString(text, 15))
        >>> alignToLeft(20)
        >>> alignToTop(20)
}
public let verificationButton: (String) -> UIButton = { text in
    return UIButton()
        |> named("Button")
        >>> utilizeConstraints
        >>> width_Height(max(50, text.rectForText(width: 300, textSize: 15).width*1.5), 50)
        >>> styledTitle(darkGreyAttributedString(text, 15))
        >>> roundedGreyShadowed(25)
        >>> asButton
}
public let backButton: UIButton =
    UIButton()
        |> named("back_button")
        >>> setImage(UIImage(named: "BackButton"))
        >>> alignToLeft(10)
        >>> alignToTop(70)
        >>> asButton

public let appLogo: UIView =
    UIImageView(image: UIImage(named: "logoDark"))
        |> width_Height(50, 50)
        >>> alignedToCenterX
        >>> alignToTop(50)
        >>> named("logo")

public enum TestMode {
    case simulatorTest
    case deviceTest
    case none
    static let mode: TestMode = .deviceTest
}
public func printLog(_ log: String ){
    switch TestMode.mode {
    case .simulatorTest:
        print(log)
    case .deviceTest:
        let modal = full_screen_modal(log)
        modal.frame = UIScreen.main.bounds
        DispatchQueue.main.async {
            currentWindow().add(views: modal)
        }
    case .none:
        break
    }
}
