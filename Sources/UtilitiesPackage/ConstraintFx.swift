//
//  ConstraintFx.swift
//  UIFunctions
//
//  Created by blakerogers on 9/18/19.
//  Copyright © 2019 com.meld.kinnectus. All rights reserved.
//

import UIKit


public extension UIView {
    private static var _constraintFx = [String : [SingleView]]()
    private static var _name = [String: String]()
    
    var constraintFx: [SingleView] {
        get {
            let tmpAddress = String(format: "%p", unsafeBitCast(self, to: Int.self))
            return UIView._constraintFx[tmpAddress] ?? []
        }
        set(newValue) {
            let tmpAddress = String(format: "%p", unsafeBitCast(self, to: Int.self))
            UIView._constraintFx[tmpAddress] = newValue
        }
    }
   var name: String {
        get {
            let tmpAddress = String(format: "%p", unsafeBitCast(self, to: Int.self))
            return UIView._name[tmpAddress] ?? ""
        }
        set(newValue) {
            let tmpAddress = String(format: "%p", unsafeBitCast(self, to: Int.self))
            UIView._name[tmpAddress] = newValue
        }
    }
    func viewName() -> String {
        return name
    }
}


public let fillOffsetByMargin: (CGFloat) -> View = { margin in
    return alignToRight(-margin) >>> alignToLeft(margin) >>> alignToTop(margin) >>> alignToBottom(-margin)
}
public let fill: View = alignedToRight >>> alignedToLeft >>> alignedToTop >>> alignedToBottom
public let alignLeftToView: (UIView, CGFloat) -> View = { siblingView, dx in
    return { view in
        view.constrainLeftToLeft(of: siblingView, constant: dx)
        return view
    }
}
public let alignTopToView: (UIView, CGFloat) -> View = { siblingView, dy in
    return { view in
        view.constrainTopToTop(of: siblingView, constant: dy)
        return view
    }
}
public let alignRightToView: (UIView, CGFloat) -> View = { siblingView, dx in
    return { view in
        view.constrainRightToRight(of: siblingView, constant: dx)
        return view
    }
}
public let alignBottomToView: (UIView, CGFloat) -> View = { siblingView, dy in
    return { view in
        view.constrainBottomToBottom(of: siblingView, constant: dy)
        return view
    }
}
public let sameWidth: View = { view in
    view.translatesAutoresizingMaskIntoConstraints = false
    view.constraintFx.append({ view in
        guard let superview = view.superview else { fatalError("Missing superview")}
        view.widthAnchor.constraint(equalTo: superview.widthAnchor).isActive = true
    })
    return view
}
public let sameHeight: View = { view in
    view.translatesAutoresizingMaskIntoConstraints = false
    view.constraintFx.append({ view in
        guard let superview = view.superview else { fatalError("Missing superview")}
        view.heightAnchor.constraint(equalTo: superview.heightAnchor).isActive = true
    })
    return view
}
public let sameSize: View = { view in
    view.translatesAutoresizingMaskIntoConstraints = false
    view.constraintFx.append({ view in
        guard let superview = view.superview else { fatalError("Missing superview")}
        view.heightAnchor.constraint(equalTo: superview.heightAnchor).isActive = true
        view.widthAnchor.constraint(equalTo: superview.widthAnchor).isActive = true
    })
    return view
}
public func alignToTop(_ dy: CGFloat = 0) -> View {
    return { view in
        view.translatesAutoresizingMaskIntoConstraints = false
        view.constraintFx.append({ view in
            guard let superview = view.superview else { fatalError("Missing superview")}
            view.topAnchor.constraint(equalTo: superview.topAnchor, constant: dy).isActive = true
        })
        return view
    }
}
public let alignedToTop: View = { view in
    view.translatesAutoresizingMaskIntoConstraints = false
    view.constraintFx.append({ view in
        guard let superview = view.superview else { fatalError("Missing superview")}
        view.topAnchor.constraint(equalTo: superview.topAnchor, constant: 0).isActive = true
    })
    return view
}

public let alignedToSafeAreaTop: View = { view in
        view.translatesAutoresizingMaskIntoConstraints = false
        view.constraintFx.append({ view in
            guard let superview = view.superview else { fatalError("Missing superview") }
            if #available(iOS 11.0, *) {
                view.topAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.topAnchor).isActive = true
            } else {
                // Fallback on earlier versions
                view.topAnchor.constraint(equalTo: superview.topAnchor).isActive = true
            }
        })
        return view
}

public func alignTopToBottom(_ dy: CGFloat) -> View {
    return { view in
        view.translatesAutoresizingMaskIntoConstraints = false
        view.constraintFx.append({ view in
            guard let superview = view.superview else { fatalError("Missing superview")}
            view.topAnchor.constraint(equalTo: superview.bottomAnchor, constant: dy).isActive = true
        })
        return view
    }
}
public let alignedTopToBottom:  View = { view in
    view.translatesAutoresizingMaskIntoConstraints = false
    view.constraintFx.append({ view in
        guard let superview = view.superview else { fatalError("Missing superview")}
        view.topAnchor.constraint(equalTo: superview.bottomAnchor, constant: 0).isActive = true
    })
    return view
}
public func alignTopToCenter(_ dy: CGFloat = 0) -> View {
    return { view in
        view.translatesAutoresizingMaskIntoConstraints = false
        view.constraintFx.append({ view in
            guard let superview = view.superview else { fatalError("Missing superview")}
            view.topAnchor.constraint(equalTo: superview.centerYAnchor, constant: dy).isActive = true
        })
        return view
    }
}
public let alignedTopToCenter: View = { view in
    view.translatesAutoresizingMaskIntoConstraints = false
    view.constraintFx.append({ view in
    guard let superview = view.superview else { fatalError("Missing superview")}
    view.topAnchor.constraint(equalTo: superview.centerYAnchor, constant: 0).isActive = true
    })
    return view
}
public func alignToBottom(_ dy: CGFloat = 0) -> View {
    return { view in
        view.translatesAutoresizingMaskIntoConstraints = false
        view.constraintFx.append({ view in
            guard let superview = view.superview else { fatalError("Missing superview")}
            view.bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: dy).isActive = true
        })
        return view
    }
}
public let alignedToBottom: View = { view in
    view.translatesAutoresizingMaskIntoConstraints = false
    view.constraintFx.append({ view in
        guard let superview = view.superview else { fatalError("Missing superview")}
        view.bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: 0).isActive = true
    })
    return view
}
public func alignBottomToTop(_ dy: CGFloat = 0) -> View {
    return { view in
        view.translatesAutoresizingMaskIntoConstraints = false
        view.constraintFx.append({ view in
            guard let superview = view.superview else { fatalError("Missing superview")}
            view.bottomAnchor.constraint(equalTo: superview.topAnchor, constant: dy).isActive = true
        })
        return view
    }
}
public let alignedBottomToTop: View = { view in
    view.translatesAutoresizingMaskIntoConstraints = false
    view.constraintFx.append({ view in
        guard let superview = view.superview else { fatalError("Missing superview")}
        view.bottomAnchor.constraint(equalTo: superview.topAnchor, constant: 0).isActive = true
    })
    return view
}
public func alignBottomToCenter(_ dy: CGFloat = 0) -> View {
    return { view in
        view.translatesAutoresizingMaskIntoConstraints = false
        view.constraintFx.append({ view in
            guard let superview = view.superview else { fatalError("Missing superview")}
            view.bottomAnchor.constraint(equalTo: superview.centerYAnchor, constant: dy).isActive = true
        })
        return view
    }
}
public let alignedBottomToCenter: View = { view in
    view.translatesAutoresizingMaskIntoConstraints = false
    view.constraintFx.append({ view in
        guard let superview = view.superview else { fatalError("Missing superview")}
        view.bottomAnchor.constraint(equalTo: superview.centerYAnchor, constant: 0).isActive = true
    })
    return view
}
public func alignToLeft(_ dx: CGFloat = 0) -> View {
    return { view in
        view.translatesAutoresizingMaskIntoConstraints = false
        view.constraintFx.append({ view in
            guard let superview = view.superview else { fatalError("Missing superview")}
            view.leftAnchor.constraint(equalTo: superview.leftAnchor, constant: dx).isActive = true
        })
        return view
    }
}
public let alignedToLeft: View = { view in
    view.translatesAutoresizingMaskIntoConstraints = false
    view.constraintFx.append({ view in
        guard let superview = view.superview else { fatalError("Missing superview")}
        view.leftAnchor.constraint(equalTo: superview.leftAnchor, constant: 0).isActive = true
    })
    return view
}
public func alignLeftToRight(_ dx: CGFloat = 0) -> View {
    return { view in
        view.translatesAutoresizingMaskIntoConstraints = false
        view.constraintFx.append({ view in
            guard let superview = view.superview else { fatalError("Missing superview")}
            view.leftAnchor.constraint(equalTo: superview.rightAnchor, constant: dx).isActive = true
        })
        return view
    }
}
public func alignLeftToCenter(_ dx: CGFloat = 0) -> View {
    return { view in
        view.translatesAutoresizingMaskIntoConstraints = false
        view.constraintFx.append({ view in
            guard let superview = view.superview else { fatalError("Missing superview")}
            view.leftAnchor.constraint(equalTo: superview.centerXAnchor, constant: dx).isActive = true
        })
        return view
    }
}
public func alignToRight(_ dx: CGFloat = 0) -> View {
    return { view in
        view.translatesAutoresizingMaskIntoConstraints = false
        view.constraintFx.append({ view in
            guard let superview = view.superview else { fatalError("Missing superview")}
            view.rightAnchor.constraint(equalTo: superview.rightAnchor, constant: dx).isActive = true
        })
        return view
    }
}
public let alignedToRight: View = { view in
    view.translatesAutoresizingMaskIntoConstraints = false
    view.constraintFx.append({ view in
        guard let superview = view.superview else { fatalError("Missing superview")}
        view.rightAnchor.constraint(equalTo: superview.rightAnchor, constant: 0).isActive = true
    })
    return view
}
public func alignRightToCenter(_ dx: CGFloat = 0) -> View {
    return { view in
        view.translatesAutoresizingMaskIntoConstraints = false
        view.constraintFx.append({ view in
            guard let superview = view.superview else { fatalError("Missing superview")}
            view.rightAnchor.constraint(equalTo: superview.centerXAnchor, constant: dx).isActive = true
        })
        return view
    }
}
public func alignRightToLeft(_ dx: CGFloat = 0) -> View {
    return { view in
        view.translatesAutoresizingMaskIntoConstraints = false
        view.constraintFx.append({ view in
            guard let superview = view.superview else { fatalError("Missing superview")}
            view.rightAnchor.constraint(equalTo: superview.leftAnchor, constant: dx).isActive = true
        })
        return view
    }
}
public func alignToCenterX(_ dx: CGFloat = 0) -> View {
    return { view in
        view.translatesAutoresizingMaskIntoConstraints = false
        view.constraintFx.append({ view in
            guard let superview = view.superview else { fatalError("Missing superview")}
            view.centerXAnchor.constraint(equalTo: superview.centerXAnchor, constant: dx).isActive = true
        })
        return view
    }
}
public let alignedToCenterX: View = { view in
    view.translatesAutoresizingMaskIntoConstraints = false
    view.constraintFx.append({ view in
        guard let superview = view.superview else { fatalError("Missing superview")}
        view.centerXAnchor.constraint(equalTo: superview.centerXAnchor, constant: 0).isActive = true
    })
    return view
}
public func alignCenterXToLeft(_ dx: CGFloat = 0) -> View {
    return { view in
        view.translatesAutoresizingMaskIntoConstraints = false
        view.constraintFx.append({ view in
            guard let superview = view.superview else { fatalError("Missing superview")}
            view.centerXAnchor.constraint(equalTo: superview.leftAnchor, constant: dx).isActive = true
        })
        return view
    }
}
public func alignCenterXToRight(_ dx: CGFloat = 0) -> View {
    return { view in
        view.translatesAutoresizingMaskIntoConstraints = false
        view.constraintFx.append({ view in
            guard let superview = view.superview else { fatalError("Missing superview")}
            view.centerXAnchor.constraint(equalTo: superview.rightAnchor, constant: dx).isActive = true
        })
        return view
    }
}
public func alignToCenterY(_ dy: CGFloat = 0) -> View {
    return { view in
        view.translatesAutoresizingMaskIntoConstraints = false
        view.constraintFx.append({ view in
            guard let superview = view.superview else { fatalError("Missing superview")}
            view.centerYAnchor.constraint(equalTo: superview.centerYAnchor, constant: dy).isActive = true
        })
        return view    }
}
public let alignedToCenter: View  = alignedToCenterX >>> alignedToCenterY

public let alignedToCenterY :View = { view in
    view.translatesAutoresizingMaskIntoConstraints = false
    view.constraintFx.append({ view in
        guard let superview = view.superview else { fatalError("Missing superview")}
        view.centerYAnchor.constraint(equalTo: superview.centerYAnchor, constant: 0).isActive = true
    })
    return view
}
public func alignCenterYToTop(_ dy: CGFloat = 0) -> View {
    return { view in
        view.translatesAutoresizingMaskIntoConstraints = false
        view.constraintFx.append({ view in
            guard let superview = view.superview else { fatalError("Missing superview")}
            view.centerYAnchor.constraint(equalTo: superview.topAnchor, constant: dy).isActive = true
        })
        return view
    }
}
public func alignCenterYToBottom(_ dy: CGFloat = 0) -> View {
    return { view in
        view.translatesAutoresizingMaskIntoConstraints = false
        view.constraintFx.append({ view in
            guard let superview = view.superview else { fatalError("Missing superview")}
            view.centerYAnchor.constraint(equalTo: superview.bottomAnchor, constant: dy).isActive = true
        })
        return view
    }
}
public func width_Height(_ width: CGFloat, _ height: CGFloat) -> View {
    return { view in
        view.translatesAutoresizingMaskIntoConstraints = false
        view.widthAnchor.constraint(equalToConstant: width).isActive = true
        view.heightAnchor.constraint(equalToConstant: height).isActive = true
        return view
    }
}
public func set_width_Height(_ width: CGFloat, _ height: CGFloat) -> SingleView {
    return { view in
        view.translatesAutoresizingMaskIntoConstraints = false
        view.widthAnchor.constraint(equalToConstant: width).isActive = true
        view.heightAnchor.constraint(equalToConstant: height).isActive = true
    }
}
public func viewWidth(_ width: CGFloat) -> View {
    return { view in
        view.translatesAutoresizingMaskIntoConstraints = false
        view.widthAnchor.constraint(equalToConstant: width).isActive = true
        return view
    }
}
public func viewHeight(_ height: CGFloat) -> View {
    return { view in
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: height).isActive = true
        return view
    }
}
public func width(_ width: CGFloat) -> View {
    return { view in
        view.translatesAutoresizingMaskIntoConstraints = false
        view.widthAnchor.constraint(equalToConstant: width).isActive = true
        return view
    }
}
public func height(_ height: CGFloat) -> View {
    return { view in
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: height).isActive = true
        return view
    }
}
public func ifView(condition: Bool, _ conditionalView: @escaping View) -> View {
    return { view in
        if condition {
             return view |> conditionalView
        } else {
            return view
        }
    }
}

public let isSibling: (UIView) -> (UIView) ->  Bool = { sibling in
    return { view in
        return view.superview == sibling.superview
    }
}

//Sibling Constraints
public func alignToSibling(_ named: String, constraint: @escaping DoubleView) -> View {
    return { view in
        view.constraintFx.append({ _ in
            guard let superview = view.superview else { fatalError("No superview")}
            constraint(view, superview |> child(named))
        })
        return view
    }
}
public let fillSibling: DoubleView = { view, sibling in
    guard view |> isSibling(sibling) else { assertionFailure("Not in view hierarchy"); return }
    view.translatesAutoresizingMaskIntoConstraints = false
    view.topAnchor.constraint(equalTo: sibling.topAnchor).isActive = true
    view.leftAnchor.constraint(equalTo: sibling.leftAnchor).isActive = true
    view.rightAnchor.constraint(equalTo: sibling.rightAnchor).isActive = true
    view.bottomAnchor.constraint(equalTo: sibling.bottomAnchor).isActive = true

}
public let alignedToSiblingCenter: DoubleView = { view, sibling in
        view.translatesAutoresizingMaskIntoConstraints = false
        view.constraintFx.append({ view in
            if view |> isSibling(sibling) {
                view.centerXAnchor.constraint(equalTo: sibling.centerXAnchor, constant: 0).isActive = true
                view.centerYAnchor.constraint(equalTo: sibling.centerYAnchor, constant: 0).isActive = true
            } else {
                view.superview?.addSubview(sibling)
                view.centerXAnchor.constraint(equalTo: sibling.centerXAnchor, constant: 0).isActive = true
                view.centerYAnchor.constraint(equalTo: sibling.centerYAnchor, constant: 0).isActive = true
            }
        })
}
public let alignedToSiblingCenterX: DoubleView = { view, sibling in
        view.translatesAutoresizingMaskIntoConstraints = false
        view.constraintFx.append({ view in
            guard view |> isSibling(sibling) else { assertionFailure("Not in view hierarchy"); return }
            view.centerXAnchor.constraint(equalTo: sibling.centerXAnchor, constant: 0).isActive = true
        })
}
public let alignedToSiblingCenterY: DoubleView = { view, sibling in
        view.translatesAutoresizingMaskIntoConstraints = false
        view.constraintFx.append({ view in
            guard view |> isSibling(sibling) else { assertionFailure("Not in view hierarchy"); return }
            view.centerYAnchor.constraint(equalTo: sibling.centerYAnchor, constant: 0).isActive = true
        })
}
public func alignToSiblingTop(_ dy: CGFloat = 0) -> DoubleView {
    return { view, sibling in
        view.translatesAutoresizingMaskIntoConstraints = false
        view.constraintFx.append({ view in
            guard view |> isSibling(sibling) else { assertionFailure("Not in view hierarchy"); return }
            print(view.name)
            print(sibling.name)
            view.topAnchor.constraint(equalTo: sibling.topAnchor, constant: dy).isActive = true
        })
    }
}
public func alignTopToSiblingBottom(_ dy: CGFloat = 0) -> DoubleView {
    return { view, sibling in
        guard view |> isSibling(sibling) else { assertionFailure("Not in view hierarchy"); return }
        view.translatesAutoresizingMaskIntoConstraints = false
        view.topAnchor.constraint(equalTo: sibling.bottomAnchor, constant: dy).isActive = true
    }
}
public let alignedTopToSiblingBottom: DoubleView = { view, sibling in
    guard view |> isSibling(sibling) else { assertionFailure("Not in view hierarchy"); return }
    view.translatesAutoresizingMaskIntoConstraints = false
    view.topAnchor.constraint(equalTo: sibling.bottomAnchor, constant: 0).isActive = true
}
public func alignTopToSiblingCenter(_ dy: CGFloat = 0) -> DoubleView {
    return { view, sibling in
        guard view |> isSibling(sibling) else { assertionFailure("Not in view hierarchy"); return }
        view.translatesAutoresizingMaskIntoConstraints = false
        view.topAnchor.constraint(equalTo: sibling.centerYAnchor, constant: dy).isActive = true
    }
}
public func alignToSiblingBottom(_ dy: CGFloat = 0) -> DoubleView {
    return { view, sibling in
        guard view |> isSibling(sibling) else { assertionFailure("Not in view hierarchy"); return }
        view.translatesAutoresizingMaskIntoConstraints = false
        view.bottomAnchor.constraint(equalTo: sibling.bottomAnchor, constant: dy).isActive = true
    }
}
public func alignBottomToSiblingTop(_ dy: CGFloat = 0) -> DoubleView {
    return { view, sibling in
        guard view |> isSibling(sibling) else { assertionFailure("Not in view hierarchy"); return }
        view.translatesAutoresizingMaskIntoConstraints = false
        view.bottomAnchor.constraint(equalTo: sibling.topAnchor, constant: dy).isActive = true
    }
}
public let alignedToSiblingTop: DoubleView = { view, sibling in
    guard view |> isSibling(sibling) else { assertionFailure("Not in view hierarchy"); return }
    view.translatesAutoresizingMaskIntoConstraints = false
    view.topAnchor.constraint(equalTo: sibling.topAnchor, constant: 0).isActive = true
}
public let alignedToSiblingBottom: DoubleView = { view, sibling in
    guard view |> isSibling(sibling) else { assertionFailure("Not in view hierarchy"); return }
    view.translatesAutoresizingMaskIntoConstraints = false
    view.bottomAnchor.constraint(equalTo: sibling.bottomAnchor, constant: 0).isActive = true
}
public func alignBottomToSiblingCenter(_ dy: CGFloat = 0) -> DoubleView {
    return { view, sibling in
        guard view |> isSibling(sibling) else { assertionFailure("Not in view hierarchy"); return }
        view.translatesAutoresizingMaskIntoConstraints = false
        view.bottomAnchor.constraint(equalTo: sibling.centerYAnchor, constant: dy).isActive = true
    }
}
public func alignToSiblingLeft(_ dx: CGFloat = 0) -> DoubleView {
    return { view, sibling in
        guard view |> isSibling(sibling) else { assertionFailure("Not in view hierarchy"); return }
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leftAnchor.constraint(equalTo: sibling.leftAnchor, constant: dx).isActive = true
    }
}
public let alignedToSiblingLeft: DoubleView = { view, sibling in
    guard view |> isSibling(sibling) else { assertionFailure("Not in view hierarchy"); return }
    view.translatesAutoresizingMaskIntoConstraints = false
    view.leftAnchor.constraint(equalTo: sibling.leftAnchor, constant: 0).isActive = true
}
public let alignedToSiblingRight: DoubleView = { view, sibling in
    guard view |> isSibling(sibling) else { assertionFailure("Not in view hierarchy"); return }
    view.translatesAutoresizingMaskIntoConstraints = false
    view.rightAnchor.constraint(equalTo: sibling.rightAnchor, constant: 0).isActive = true
}
public func alignLeftToSiblingRight(_ dx: CGFloat = 0) -> DoubleView {
    return { view, sibling in
        guard view |> isSibling(sibling) else { assertionFailure("Not in view hierarchy"); return }
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leftAnchor.constraint(equalTo: sibling.rightAnchor, constant: dx).isActive = true
    }
}
public let alignedLeftToSiblingRight: DoubleView = { view, sibling in
    guard view |> isSibling(sibling) else { assertionFailure("Not in view hierarchy"); return }
    view.translatesAutoresizingMaskIntoConstraints = false
    view.leftAnchor.constraint(equalTo: sibling.rightAnchor, constant: 0).isActive = true
}
public func alignLeftToSiblingCenter(_ dx: CGFloat = 0) -> DoubleView {
    return { view, sibling in
        guard view |> isSibling(sibling) else { assertionFailure("Not in view hierarchy"); return }
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leftAnchor.constraint(equalTo: sibling.centerXAnchor, constant: dx).isActive = true
    }
}
public func alignToSiblingRight(_ dx: CGFloat = 0) -> DoubleView {
    return { view, sibling in
        guard view |> isSibling(sibling) else { assertionFailure("Not in view hierarchy"); return }
        view.translatesAutoresizingMaskIntoConstraints = false
        view.rightAnchor.constraint(equalTo: sibling.rightAnchor, constant: dx).isActive = true
    }
}
public func alignRightToSiblingCenter(_ dx: CGFloat = 0) -> DoubleView {
    return { view, sibling in
        guard view |> isSibling(sibling) else { assertionFailure("Not in view hierarchy"); return }
        view.translatesAutoresizingMaskIntoConstraints = false
        view.rightAnchor.constraint(equalTo: sibling.centerXAnchor, constant: dx).isActive = true
    }
}
public func alignRightToSiblingLeft(_ dx: CGFloat = 0) -> DoubleView {
    return { view, sibling in
        guard view |> isSibling(sibling) else { assertionFailure("Not in view hierarchy"); return }
        view.translatesAutoresizingMaskIntoConstraints = false
        view.rightAnchor.constraint(equalTo: sibling.leftAnchor, constant: dx).isActive = true
    }
}
public func alignToSiblingCenterX(_ dx: CGFloat = 0) -> DoubleView {
    return { view, sibling in
        guard view |> isSibling(sibling) else { assertionFailure("Not in view hierarchy"); return }
        view.translatesAutoresizingMaskIntoConstraints = false
        view.centerXAnchor.constraint(equalTo: sibling.centerXAnchor, constant: dx).isActive = true
    }
}
public func alignCenterXToSiblingLeft(_ dx: CGFloat = 0) -> DoubleView {
    return { view, sibling in
        guard view |> isSibling(sibling) else { assertionFailure("Not in view hierarchy"); return }
        view.translatesAutoresizingMaskIntoConstraints = false
        view.centerXAnchor.constraint(equalTo: sibling.leftAnchor, constant: dx).isActive = true
    }
}
public func alignCenterXToSiblingRight(_ dx: CGFloat = 0) -> DoubleView {
    return { view, sibling in
        guard view |> isSibling(sibling) else { assertionFailure("Not in view hierarchy"); return }
        view.translatesAutoresizingMaskIntoConstraints = false
        view.centerXAnchor.constraint(equalTo: sibling.rightAnchor, constant: dx).isActive = true
    }
}
public func alignToSiblingCenterY(_ dy: CGFloat = 0) -> DoubleView {
    return { view, sibling in
        guard view |> isSibling(sibling) else { assertionFailure("Not in view hierarchy"); return }
        view.translatesAutoresizingMaskIntoConstraints = false
        view.centerYAnchor.constraint(equalTo: sibling.centerYAnchor, constant: dy).isActive = true
    }
}
public func alignCenterYToSiblingTop(_ dy: CGFloat = 0) -> DoubleView {
    return { view, sibling in
        guard view |> isSibling(sibling) else { assertionFailure("Not in view hierarchy"); return }
        view.translatesAutoresizingMaskIntoConstraints = false
        view.centerYAnchor.constraint(equalTo: sibling.topAnchor, constant: dy).isActive = true
    }
}
public func alignCenterYToSiblingBottom(_ dy: CGFloat = 0) -> DoubleView {
    return { view, sibling in
        guard view |> isSibling(sibling) else { assertionFailure("Not in view hierarchy"); return }
        view.translatesAutoresizingMaskIntoConstraints = false
        view.centerYAnchor.constraint(equalTo: sibling.bottomAnchor, constant: dy).isActive = true
    }
}

public func verifyEqualTrailingConstraintToSuperview(_ view: UIView, _ dx: CGFloat) {
    let containsConstraint = view.constraintsAffectingLayout(for: .horizontal).contains(NSLayoutConstraint(item: view, attribute: .trailing, relatedBy: .equal, toItem: view.superview, attribute: .trailing, multiplier: 1.0, constant: dx))
    assert(containsConstraint, "No Constraint")
}
public func verifyEqualLeadingConstraintToSuperview(_ view: UIView, _ dx: CGFloat) {
    let containsConstraint = view.constraintsAffectingLayout(for: .horizontal).contains(NSLayoutConstraint(item: view, attribute: .leading, relatedBy: .equal, toItem: view.superview, attribute: .leading, multiplier: 1.0, constant: dx))
    assert(containsConstraint, "No Constraint")
}
public func verifyEqualTopConstraintToSuperview(_ view: UIView, _ dx: CGFloat) {
    let containsConstraint = view.constraintsAffectingLayout(for: .vertical).contains(NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: view.superview, attribute: .top, multiplier: 1.0, constant: dx))
    assert(containsConstraint, "No Constraint")
}
public func verifyEqualBottomConstraintToSuperview(_ view: UIView, _ dx: CGFloat) {
    let containsConstraint = view.constraintsAffectingLayout(for: .vertical).contains(NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: view.superview, attribute: .bottom, multiplier: 1.0, constant: dx))
    assert(containsConstraint, "No Constraint")
}
public let alignedToLeftTop: View = alignedToLeft >>> alignedToTop
public let alignedToLeftTopRight: View = alignedToLeft >>> alignedToTop >>> alignedToRight
public let alignedToLeftBottomRight: View = alignedToLeft >>> alignedToBottom >>> alignedToRight
public let aligedToLeftBottom: View = alignedToLeft >>> alignedToBottom
public let alignedToLeftRight: View = alignedToLeft >>> alignedToRight
public let alignedToRightTop: View = alignedToRight >>> alignedToTop
public let alignedToRightBottom: View = alignedToRight >>> alignedToBottom
public let alignedToTopBottom: View = alignedToTop >>> alignedToBottom
public let insetEdges: (CGFloat) -> View = { inset in
    return { view in
        view
        |> alignToLeft(inset)
        >>> alignToTop(inset)
        >>> alignToRight(-inset)
        >>> alignToBottom(-inset)
    }
}
