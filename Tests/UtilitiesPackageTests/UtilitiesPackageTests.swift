import XCTest
@testable import UtilitiesPackage

final class UtilitiesPackageTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(UtilitiesPackage().text, "Hello, World!")
    }
}
